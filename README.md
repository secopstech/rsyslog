Overview
========

This is an alpine based minimal rsyslog server that listens UDP/TCP 514 and sends logs
to elasticsearch server/cluster...

You need to pass INDEX_TEMPLATE, INDEX_PREFIX, ES_SERVER, ES_SERVER_PORT vars
in order to ship logs to your elasticsearch as follow:

```
docker run --name my-rsyslog -d \
   -p 514:514/tcp \
   -p 514:514/udp \
   --env="INDEX_TEMPLATE=baseTemplate" \
   --env="INDEX_PREFIX=logs" \
   --env="ES_SERVER=my-es" \
   --env="ES_SERVER_PORT=9200" \
   secopstech/rsyslog
```