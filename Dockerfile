FROM alpine:3.4

MAINTAINER Cagri Ersen <cagri.ersen@secopstech.io>

ENV RVERSION 8.22.0

RUN apk --no-cache --update add curl curl-dev build-base linux-headers bsd-compat-headers \
   openssl openssl-dev libestr-dev util-linux-dev util-linux-dev \
   libgcrypt-dev liblogging-dev libfastjson-dev && \
   curl http://www.rsyslog.com/files/download/rsyslog/rsyslog-${RVERSION}.tar.gz -o /tmp/rsyslog-${RVERSION}.tar.gz && \
   cd /tmp && tar xvfz rsyslog-${RVERSION}.tar.gz && cd rsyslog-${RVERSION} && \
   ./configure --enable-imptcp --enable-elasticsearch --enable-imptcp && \
   make && make install && \
   cd / && \
   apk --update del curl-dev build-base linux-headers && \
   rm -rf /tmp/rsyslog-*

COPY ./scripts /scripts
COPY ./etc /etc
RUN mkdir /etc/rsyslog.d/ && chmod +x /scripts/* && /scripts/dynamic-conf-init

EXPOSE 514 514/tcp
EXPOSE 514 514/udp

ENTRYPOINT ["rsyslogd", "-n", "-f", "/etc/rsyslog.conf"]
